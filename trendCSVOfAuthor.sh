#!/bin/sh

#default eprints
echo "Please enter the database name of the eprints database: "
read dbname
echo "Using database named: $dbname"

#default thom
echo "Please enter the username of the $dbname database: "
read username
echo "Using username: $username for database: $dbname"

#default "/Applications/Postgres.app/Contents/Versions/9.4/bin/psql"
echo "Please enter the path to the psql binary: "
read psqlbin
echo "Using psql binary: $psqlbin"

#default Nijholt A.
echo "Please enter the exact name of the author as written in the papers: "
read theauthor
echo "Creating trend for author: $theauthor"
author_name="'"$theauthor"'"
#all
#group past < 2011
#group present >= 2011 <= 2013
#group future > 2013

$psqlbin $dbname $username -t << EOF

Copy (SELECT EXTRACT(YEAR FROM p.published), COUNT(*)-1 as collabs, COUNT(DISTINCT p.pid) FROM papers p LEFT JOIN writers w ON p.pid = w.pid LEFT JOIN writers k ON k.pid = w.pid WHERE w.aid = (SELECT aid FROM authors WHERE name = $author_name) GROUP BY EXTRACT(YEAR FROM p.published)) To '/tmp/output_trend.csv' With CSV;

EOF
echo 'Result in: /tmp/output_trend.csv'
