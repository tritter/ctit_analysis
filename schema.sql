BEGIN TRANSACTION;

CREATE TABLE papers (
    pid integer PRIMARY KEY,
    title text,
    published date
);
CREATE TABLE authors (
    aid integer PRIMARY KEY,
    name text
);
CREATE TABLE writers (
	id serial PRIMARY KEY,
    pid integer REFERENCES papers(pid),
    aid integer REFERENCES authors(aid)
);

create index idx_papers_pid on papers (pid);
create index idx_authors_aid on authors (aid);

ANALYZE;
COMMIT;