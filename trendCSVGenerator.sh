#!/bin/sh

dbname="eprints"
username="thom"
psqlbin="/Applications/Postgres.app/Contents/Versions/9.4/bin/psql"

#all
#group past < 2011
#group present >= 2011 <= 2013
#group future > 2013

$psqlbin $dbname $username -t << EOF
Copy (SELECT aid, (SELECT COUNT(*) FROM writers w WHERE a.aid = w.aid),(SELECT SUM((SELECT COUNT(*) FROM writers r WHERE r.pid = p.pid AND r.aid != p.aid)) FROM writers p WHERE a.aid = p.aid) FROM authors a) To '/tmp/output_all.csv' With CSV;

Copy (SELECT EXTRACT(YEAR FROM p.published), COUNT(*)-1 as collabs, COUNT(DISTINCT p.pid) FROM papers p LEFT JOIN writers w ON p.pid = w.pid LEFT JOIN writers k ON k.pid = w.pid WHERE w.aid = 302 GROUP BY EXTRACT(YEAR FROM p.published)) To '/tmp/output_trend.csv' With CSV;

EOF
