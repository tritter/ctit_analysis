#!/bin/sh

dbname="eprints"
username="thom"
psqlbin="/Applications/Postgres.app/Contents/Versions/9.4/bin/psql"
echo '<?xml version="1.0" encoding="UTF-8"?>
<graphml xmlns="http://graphml.graphdrawing.org/xmlns"  
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://graphml.graphdrawing.org/xmlns/1.0/graphml.xsd">
  
  <key id="name" for="node" attr.name="label" attr.type="string">
	<default>no name</default>
  </key>
  <key id="number_of_papers" for="node" attr.name="weight" attr.type="integer">
	<default>0</default>
  </key>
  <graph id="G" edgedefault="undirected">'
$psqlbin $dbname $username -t << EOF
SELECT xmlelement(name node, xmlattributes(aid as id), xmlconcat(xmlelement(name data, xmlattributes('name' as key), name)), xmlconcat(xmlelement(name data, xmlattributes('number_of_papers' as key), (SELECT COUNT(*) FROM writers w WHERE a.aid = w.aid)))) FROM authors a;
EOF
$psqlbin $dbname $username -t << EOF
SELECT xmlelement(name edge, xmlattributes( 'e'||w.aid||(SELECT EXTRACT(MILLISECONDS FROM NOW())) as id, p.aid as source, w.aid as target)) FROM writers p LEFT JOIN writers w ON p.pid = w.pid WHERE p.aid != w.aid GROUP BY p.aid,w.aid;
EOF
echo '</graph>
</graphml>'
