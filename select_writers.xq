let $doc := doc("eprints.xml")
let $before_statement := "INSERT INTO &#34;writers&#34; (pid,aid) VALUES("
let $after_statement := ");"
let $all_authors := distinct-values($doc//author)
for $paper in  $doc//paper
  let $id := $paper/eprintid[text()]
  for $author in  $paper//author
    let $author_id := index-of($all_authors,$author)
    return concat($before_statement, $id,",",$author_id,$after_statement)
