let $doc := doc("eprints.xml")
let $before_statement := "INSERT INTO &#34;papers&#34; (pid,published,title) VALUES("
let $after_statement := ");"
for $paper in  $doc//paper
let $id := $paper/eprintid[text()]
let $title := $paper/title[text()]
let $datestamp := $paper/datestamp[text()]
return concat($before_statement,$id,",'",$datestamp,"','",replace($title, '''', ''''''),"'",$after_statement)