let $doc := doc("eprints.xml")
let $before_statement := "INSERT INTO &#34;authors&#34; (aid,name) VALUES("
let $after_statement := ");"
for $author at $i in distinct-values($doc//author)
return concat( $before_statement, $i,",'",replace($author, '''', ''''''),"'",$after_statement)